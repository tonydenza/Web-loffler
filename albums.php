<?php
    include("./connection.php");
    session_start();
    $q = 1; $p = 1; $j = 1;
    for($i=3;$i>=1;$i--) {
        $albums = "SELECT id, name, year, songs FROM albums";
        $result = mysqli_query($conn, $albums);

        if (mysqli_num_rows($result)) {
            while ($row = mysqli_fetch_assoc($result)) {
                $id[$p] = $row["id"];
                $name_a[$p] = $row["name"];
                $year[$p] = $row["year"];
                $songs_n[$p] = $row["songs"];
                $p++;
            }
        }

        $last_song[3] = 17; $last_song[2] = 25; $last_song[1] = 37;
        $songs = "SELECT no, name, lenght FROM songs WHERE album=$i";
        $result = mysqli_query($conn, $songs);

        if (mysqli_num_rows($result)) {
            while ($row = mysqli_fetch_assoc($result)) {
                $no[$q] = $row["no"];
                $name[$q] = $row["name"];
                $lenght[$q] = $row["lenght"];
                $q++;
            }
        }
    }
?>

<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="description" content="Semestral work for subject VIA">
        <meta name="author" content="Jan Červinka, 31117">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./styles/style.css">
        <link href='https://fonts.googleapis.com/css?family=Amatic+SC' rel="stylesheet">
        <title>Christian Löffler &bull; Albums</title>
    </head>

    <body>

        <header style="background-image: url('./img/header2.jpg');">
            <h1 style="color: #f1f1f1">Christian Löffler</h1>
        </header>

        <nav>
            <ul>
                <li><a href=".">Bio</a></li>
                <li>Albums</li>
                <li><a href="gallery.php">Gallery</a></li>
                <li><a href="shop.php">Shop</a></li>
            </ul>
        </nav>

        <main>
            <h2>Albums</h2>
            <div id="albums">
                <?php // testujeme git
                    for ($i=3; $i>=1; $i--) {
                        echo "
                        <article>
                            <h3>$name_a[$i] [$year[$i]]</h3>
                            <div class='albums'>
                                <img src='./img/album$i.jpg' class='album-picture' alt='#$i album'>
                                <table>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Lenght</th> 
                                     </tr>";
                            for ($j; $j<=$last_song[$i]; $j++) {
                                echo "
                                    <tr>
                                        <td>" . $no[$j] . "</td>
                                        <td>" . $name[$j] . "</td>
                                        <td>" . $lenght[$j] . "</td>
                                    </tr>";
                            }
                            $j=$last_song[$i] + 1;
                            echo "
                                </table>
                            </div>
                        </article >";
                    }
                ?>
            </div>
            <?php
                if (!isset($_COOKIE['username'])) {
                    echo "
                        <article id=\"vote\">
                            <h2>Vote for the best album</h2>
                            <form action=\"vote.php\" method=\"get\">
                                <div class='data'>";
                                    echo "<input class='in' type='text' name='name' placeholder='username' tabindex='1' required>";
                                    echo "<input class='in' type='number' name='age' placeholder='age' tabindex='2' required>
                                </div>
                                <div class=\"voting\">";
                                    for ($i=3; $i>=1; $i--) {
                                        echo "<div class='vote-box$i'>";
                                        echo "<input type='hidden' name='vote-image$i' id='value-$i' value='0' data-sitekey='6LeepVIUAAAAADjRXSV4HKvk3k_UDDF - XBN14_Ku'>";
                                        echo "<input class='albums-vote' id='vote-image$i' type='image' src='./img/album$i.jpg' tabindex='3' alt='Choose the best album. This is number #$i.'>";
                                        echo "</div>";
                                    }
                                    echo "
                                </div>
                            </form>
                        </article>";
                }
                else {
                    echo "
                        <article id=\"voted\" name='voted'>
                            <p>Thank you for voting, " . $_COOKIE['username']. ". <br>
                            You voted for:<br>";
                            if (!isset($_COOKIE['voted-album']))
                                $path = "./img/album" . $_SESSION['voted-album'];
                            else
                                $path = "./img/album" . $_COOKIE['voted-album'];
                            echo('<img src="' . $path . '.jpg" class="albums-voted" alt="Your voted album cover."> </p>');
                        echo "</article>";
                }
            ?>
        </main>

        <footer>
            <ul>
                <li>Semestral work &bull; <a href="https://www.vsfs.cz/">VŠFS</a></li>
                <li>Jan Červinka (31117) &bull; 2018</li>
            </ul>
        </footer>
        <script src="./scripts/vote.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </body>
</html>

<?php
    $conn->close();
?>