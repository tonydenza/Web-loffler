<!doctype html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="Semestral work for subject VIA">
        <meta name="author" content="Jan Červinka, 31117">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./styles/style.css">
        <link href='https://fonts.googleapis.com/css?family=Amatic+SC' rel="stylesheet">
        <title>Christian Löffler &bull; Gallery</title>
    </head>
    <body>
        <header style="background-image: url('./img/header3.jpg');">
            <h1>Christian Löffler</h1>
        </header>

        <nav>
            <ul>
                <li><a href=".">Bio</a></li>
                <li><a href="albums.php">Albums</a></li>
                <li>Gallery</li>
                <li><a href="shop.php">Shop</a></li>
            </ul>
        </nav>

        <main>
            <h2>Gallery</h2>
            <div id="pictures">
                <?php
                    srand();
                    $rand_image = rand(1,6);
                    $rand_image = $rand_image . ".jpg";
                    echo "<img class='gallery-image' id='picture' src=\"./img/$rand_image\" alt='#$rand_image photo'>";
                ?>
            </div>
        </main>

        <footer>
            <ul>
                <li>Semestral work &bull; <a href="https://www.vsfs.cz/">VŠFS</a></li>
                <li>Jan Červinka (31117) &bull; 2018</li>
            </ul>
        </footer>
	<script src="./scripts/gallery.js"></script>
   </body>
</html>