<!doctype html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="Semestral work for subject VIA">
        <meta name="author" content="Jan Červinka, 31117">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./styles/style.css">
        <link href='https://fonts.googleapis.com/css?family=Amatic+SC' rel="stylesheet">
        <title>Christian Löffler</title>
    </head>
    <body>
        <header style="background-image: url('./img/header1.jpg');">
            <h1>Christian Löffler</h1>
        </header>

        <nav>
            <ul>
                <li>Bio</li>
                <li><a href="albums.php">Albums</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li><a href="shop.php">Shop</a></li>
            </ul>
        </nav>

        <main>
            <article>
                <h2>Biography</h2>
                <p class="perex">
                    Being asked to describe his own music, Christian Löffler states that he tries to combine melancholy with euphoria. “All my music is connected by a gloomy spirit,
                    which is minted by a warm sincerity. I try to merge all kinds of different acoustic color.
                </p>
                <p class="para">
                    Löffler started to play music by the age of 14. Living in a secluded region, lacking a musical surrounding,
                    he had to teach himself the essentials of making electronic music. Before long he developed his own deep and moving sound,
                    colored with a melancholic undertone - Music for the soul & for the body. He recalls a variety of music styles he listened to as a child and teenager
                    that were highly influential to his own development as a musician.
                </p>
                <p class="para">
                    Due to the fact that Löffler is also working as a visual artist, he’s following the same approach when making music that he’s following when painting or taking photos.
                    It is more about telling a story than making everything accessible right from the start.
                </p>
                <p class="para">
                    In the course of the production of EP’s like “Heights”, “Raise” and “Aspen”, Loeffler developed some strategies to use acoustic material and singing in his music.
                    Amongst others he samples different stringed instruments, percussion and atmospheres. He continues working with the recordings on the computer, combining, layering and alternating them.
                </p>
                <p class="para">
                    The extraordinary capacities of this young producer have kept eyes and ears closely following the fresh talent as Loeffler’s deep moving melancholic sound, is absolutely irresistible.
                </p>
                <p class="source">
                    Source: <a href="https://www.residentadvisor.net/dj/christianloffler/biography">https://www.residentadvisor.net/dj/christianloffler/biography</a>
                </p>
            </article>
        </main>

        <footer>
            <ul>
                <li>Semestral work &bull; <a href="https://www.vsfs.cz/">VŠFS</a></li>
                <li>Jan Červinka (31117) &bull; 2018</li>
            </ul>
        </footer>
    </body>
</html>