document.getElementById('picture').onclick = function() {next(this)};

function next(picture) {
    var xhttp = new XMLHttpRequest();
    var cislo = document.getElementById('picture').src.substring(33, 34);

    if (cislo >= 6)
        cislo = 1;
    else
        cislo++;

    xhttp.open("GET", "./img/" + cislo + ".jpg", true);

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById('picture').src = this.responseURL;
            document.getElementById('picture').alt = "#" + cislo + ".jpg";
        }
    };

    xhttp.send();
}