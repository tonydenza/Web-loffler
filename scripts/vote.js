name1 = "vote-image1";
name2 = "vote-image2";
name3 = "vote-image3";

document.getElementById(name1).onclick = function() {vote(name1)};
document.getElementById(name2).onclick = function() {vote(name2)};
document.getElementById(name3).onclick = function() {vote(name3)};

function vote(name) {
    if (name == "vote-image1") {
        document.getElementById(name).style.cssText = "border-bottom: 3px solid lightgreen;";
        document.getElementById("vote-image2").style.cssText = "border-bottom: 3px solid #0d2227;";
        document.getElementById("vote-image3").style.cssText = "border-bottom: 3px solid #0d2227;";
        document.getElementById("value-1").setAttribute("value", "1");
        document.getElementById("value-2").setAttribute("value", "0");
        document.getElementById("value-3").setAttribute("value", "0");
    }
    else if (name == "vote-image2") {
        document.getElementById(name).style.cssText = "border-bottom: 3px solid lightgreen;";
        document.getElementById("vote-image1").style.cssText = "border-bottom: 3px solid #0d2227;";
        document.getElementById("vote-image3").style.cssText = "border-bottom: 3px solid #0d2227;";
        document.getElementById("value-2").setAttribute("value", "1");
        document.getElementById("value-1").setAttribute("value", "0");
        document.getElementById("value-3").setAttribute("value", "0");
    }
    else if (name == "vote-image3") {
        document.getElementById(name).style.cssText = "border-bottom: 3px solid lightgreen;";
        document.getElementById("vote-image1").style.cssText = "border-bottom: 3px solid #0d2227;";
        document.getElementById("vote-image2").style.cssText = "border-bottom: 3px solid #0d2227;";
        document.getElementById("value-3").setAttribute("value", "1");
        document.getElementById("value-1").setAttribute("value", "0");
        document.getElementById("value-2").setAttribute("value", "0");
    }
}