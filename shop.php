<?php
    session_start();
?>

<!doctype html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="Semestral work for subject VIA">
        <meta name="author" content="Jan Červinka, 31117">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./styles/style.css">
        <link href='https://fonts.googleapis.com/css?family=Amatic+SC' rel="stylesheet">
        <title>Christian Löffler &bull; Shop</title>
    </head>
    <body>
    <header style="background-image: url('./img/header4.jpg');">
        <h1 style="color: #f1f1f1">Christian Löffler</h1>
    </header>

        <nav>
            <ul>
                <li><a href=".">Bio</a></li>
                <li><a href="albums.php">Albums</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li>Shop</li>
            </ul>
        </nav>

        <main>
            <h2>Shop</h2>
            <p>Unfortunately the shop is temporarily closed.</p>
            <?php
                if(isset($_SESSION['buddy'])) {
                    echo "<p>We are truly sorry, " . $_SESSION['buddy'] . ".</p>";
                }
            ?>
        </main>

        <footer>
            <ul>
                <li>Semestral work &bull; <a href="https://www.vsfs.cz/">VŠFS</a></li>
                <li>Jan Červinka (31117) &bull; 2018</li>
            </ul>
        </footer>
    </body>
</html>