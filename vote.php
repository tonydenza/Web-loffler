<?php
// zapnuti session
session_start();

// zapis do DB
include("./connection.php");

$user = strtolower($_GET['name']);
$age = $_GET['age'];

if ($_GET['vote-image1']) {
    $album = 1;
}
else if ($_GET['vote-image2']) {
    $album = 2;
}
else if ($_GET['vote-image3']) {
    $album = 3;
}

$albums = "INSERT INTO voting (user, age, album) VALUES ('$user', '$age', '$album');";
$result = mysqli_query($conn, $albums);

if (!$result) {
    header('Location: https://malw.art/loffler/albums.php');
}

mysqli_close($conn);

// zapis do souboru

$file=fopen("voting.txt","a");
if($file)
{
    $str = "$user,$age,$album\n";
    fwrite($file, $str);
    fclose($file);
}

// nastaveni cookie
if (!isset($_COOKIE['username'])) {
    setcookie("username", $_GET['name'], time()+3600, "/loffler", "malw.art", 1);
}

$_SESSION['buddy'] = $user;
$_SESSION['voted-album'] = $album;

if (!isset($_COOKIE['voted-album'])) {
    setcookie("voted-album", $album, time()+3600, "/loffler", "malw.art", 1);
}

// presmerovani zpet

header('Location: https://malw.art/loffler/albums.php#voted');

?>